from django.contrib import admin

# Register your models here.
from .models import Trains

class TrainsAdmin(admin.ModelAdmin):
    fields = ['track', 'car', 'gate', 'date', 'departure', 'arrival']

admin.site.register(Trains, TrainsAdmin)