/* eslint-disable */

import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import vuetify from "./plugins/vuetify";
import "@mdi/font/css/materialdesignicons.css";
import VueCookies from "vue-cookies";
import VueSession from "vue-session";

Vue.config.productionTip = false;
Vue.use(VueCookies);
Vue.use(VueSession);

new Vue({
  router,
  vuetify,
  store,
  render: (h) => h(App),
}).$mount("#app");
