from django.db import models
import datetime

# Create your models here.
class Trains(models.Model):
    track = models.IntegerField(default=0)
    car = models.IntegerField(default=0)
    gate = models.IntegerField(default=0)
    date = models.DateField(default=datetime.date.today)
    departure = models.TimeField(default='00:00')
    arrival = models.TimeField(default='00:00')

    class Meta:
        constraints = [
            models.UniqueConstraint(fields=['track', 'car', 'gate', 'date', 'departure', 'arrival'], name='no_duplicates')
        ]