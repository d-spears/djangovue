/* eslint-disable */

import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    tableData: [],
  },
  mutations: {
    updateTable(state, payload) {
      state.tableData = payload;
    },
  },
  actions: {
    updateTable(context, payload) {
      context.commit("updateTable", payload);
    },
  },
  modules: {},
});
