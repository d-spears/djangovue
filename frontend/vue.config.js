process.env.VUE_APP_VERSION = require("./package.json").version;
const date = new Date();
process.env.VUE_APP_BUILD_DATETIME = date.toLocaleString();
const path = require("path");

let base = {
  devServer: {
    contentBase: path.join(__dirname, "dist"),
    port: 9000,
    proxy: {
      "/api": {
        target: "http://localhost:8000",
      },
      "/accounts/": {
        target: "http://localhost:8000",
      },
      "/admin/": {
        target: "http://localhost:8000",
      },
      "/excel": {
        target: "http://localhost:8000",
        // changeOrigin: true,
        // pathRewrite: {
        //   "^/excel": "",
        // },
      },
    },
  },
  outputDir: "../front_static/",
};

if (process.env.NODE_ENV === "production") {
  // build時の設定
  Object.assign(base, {
    publicPath: "/static/",
    pages: {
      frontend: {
        entry: "src/main.js",
        template: "public/index.html",
        filename: "../templates/frontend.html",
      },
    },
  });
}

module.exports = base;
