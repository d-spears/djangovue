from django.urls import path
from django.conf.urls import url, include
from . import views

urlpatterns = [
    path('', views.index, name='index'),
    url(r'^api/excel$', views.index),
    path('accounts/', include('django.contrib.auth.urls')),
]