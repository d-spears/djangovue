from django.shortcuts import render

from django.http import HttpResponse
from django.http import JsonResponse
from rest_framework.parsers import JSONParser 
from rest_framework import status

from .models import Trains
import json
import datetime

import pandas as pd
import sqlite3
from sqlalchemy import create_engine

def index(request):
    if request.method == "GET":
        data = list(Trains.objects.values())
        return JsonResponse(data, safe=False)

    elif request.method == "POST":
        engine = create_engine('sqlite://///Users/diallo/Desktop/Vue/django/djangovue/db.sqlite3')
        
        file = request.FILES['file']
        df = pd.read_csv(file, index_col=0)
        for i in list(df["Date"]):
            formatted_date = i.replace("/", "-")
            formatted_date = formatted_date.replace(".", "-")
            formatted_date = formatted_date.replace("年", "-")
            formatted_date = formatted_date.replace("月", "-")
            formatted_date = formatted_date.replace("日", "")
            df["Date"] = df["Date"].replace([i], formatted_date)
        new_entries = []
        existing_entries = []
        trains = Trains.objects.all()
        for row in df.itertuples():
            duplicate = False
            for train in trains:
                if row[0] == train.track and row[1] == train.car and row[2] == train.gate and row[3] == str(train.date) and row[4] == str(train.departure)[:-3] and row[5] == str(train.arrival)[:-3]:
                    existing_entries.append(row)
                    duplicate = True
            if duplicate == False:
                new_entries.append(row)
        new_df = pd.DataFrame(new_entries, columns=["Track", "Car", "Gate", "Date", "Departure", "Arrival"])
        new_df.to_sql("excel_trains", engine, if_exists="append", index=False)
        # return HttpResponse("%d entr%s added. %d entr%s rejected." % (len(new_entries), "y" if len(new_entries) == 1 else "ies", len(existing_entries), "y" if len(existing_entries) == 1 else "ies"))
        return HttpResponse(json.dumps({"new": len(new_entries), "rejected": len(existing_entries)}))

    elif request.method == "DELETE":
        data = json.loads(request.body.decode("utf-8"))
        Trains.objects.filter(id=data['id']).delete()
        return HttpResponse("Deleted")